/**
 *
 * @param {Element} element
 * @param {Object} options
 * @returns {SingleSelect.refresh}
 *
 * options = {
 *      attrs: {
 *          attribute: value
 *      },
 *      direction: "rtl | ltr",
 *      onAddButtonClick: function(text, event){}
 * }
 */
function SingleSelect(element, options) {

    // Defined Elements
    var currentElement = $(element);
    var containerElement = $('<div>').addClass('form-control single-select-container');
    var headElement = $('<a>').attr('tabindex', '0').addClass('form-control single-select-head').html('<span class="labels"></span><div><i class="caret"></i></div>');
    var dropElement = $('<div>').addClass('single-select-drop');
    var searchElement = $('<div>').addClass('single-select-search');
    var inputSearchElement = $('<input type="text"/>').addClass('form-control input-sm');
    var resultsElement = $('<ul>').addClass('single-select-results');

    // Defined Variables
    var items = [];
    var activeIndex = 0;

    // Defined Functions
    var init = function() {
        searchElement.html(inputSearchElement);
        searchElement.append('<div><i class="glyphicon glyphicon-search"></i></div>');
        dropElement.append(searchElement);
        dropElement.append(resultsElement);
        containerElement.append(headElement);
        containerElement.append(dropElement);

        currentElement.removeAttr('multiple');

        if(options && options['attrs']) {
            var attrs = options['attrs'];
            for(attr in attrs) {
                if(attr === 'class') {
                    containerElement.addClass(attrs[attr]);
                    continue;
                }
                containerElement.attr(attr, attrs[attr]);
            }
        }

        containerElement.keydown(function(event) {
            if (event.keyCode === 27) {
                hide(function() {
                    headElement.focus();
                });
            }
        });

        if (options && options['direction'] && options['direction'] === 'rtl')
            containerElement.addClass('single-select-rtl');

        inputSearchElement.focusout(function() {
            if (dropElement.css('display') === 'block') {
                hide();
            }
        });

        // For Unsubmited form
        inputSearchElement.keydown(function(event) {
            var liCount = resultsElement.children('li').length;
            var resultsElementHeight = $(resultsElement).height();
            var liHeight = resultsElement.children('li.active').outerHeight(true);

            switch (Number(event.keyCode)) {
                case 13:
                    if (activeIndex === 0) {
                        active(1);
                    }
                    select(resultsElement.children('li.active'));
                    return false;
                case 40:
                    if (activeIndex >= liCount) {
                        active(1);
                    }
                    else {
                        active(activeIndex + 1);
                    }
                    resultsElement.scrollTop(liHeight * activeIndex - resultsElementHeight);
                    return false;
                case 38:
                    if (activeIndex <= 1) {
                        active(liCount);
                    }
                    else {
                        active(activeIndex - 1);
                    }
                    resultsElement.scrollTop(liHeight * activeIndex - resultsElementHeight);
                    return false;
            }
        });

        inputSearchElement.bind('input', function(event) {
            find($(this).val());
        });

        headElement.bind("click keydown", function(event) {
            if (event.keyCode === 32 || event.keyCode === 13 || event.type === 'click') {
                if (dropElement.css('display') === 'none') {
                    show();
                }
                else {
                    hide();
                }
            }
        });

        resultsElement.on('click', 'li', function() {
            select(this);
        });

        refresh();

        //Hidden Select element and insert single select component
        currentElement.css('display', 'none');
        currentElement.after(containerElement);
    };

    var collect = function() {
        //var exist;
        items = [];
        var i;
        currentElement.children('option').each(function(number, element) {
            /*
            exist = false;
            for (i = 0; i < items.length; i++) {
                if (items[i].caption === $(element).html()) {
                    exist = true;
                    break;
                }
            }
            */
            //if (!exist) {
                items.push({
                    optionIndex: number + 1,
                    caption: $(element).html(),
                    value: $(element).val(),
                    option: element,
                    selected: $(element).attr('selected') === undefined ? false : true
                });
            //}
        });
    };

    var find = function(text) {
        resultsElement.empty();

        if (!text) {
            for (var i in items) {
                var li = $('<li>');
                li.html(items[i].caption);
                li.data('option-index', items[i].optionIndex);
                resultsElement.append(li);
            }
            return;
        }

        items.forEach(function(item) {
            if (new RegExp(escape(text.toLowerCase())).test(escape(item.caption.toLowerCase()))) {
                var li = $('<li>');
                li.html(item.caption);
                li.data('option-index', item.optionIndex);
                resultsElement.append(li);
            }
        });

        if (resultsElement.children().length === 0) {
            var li = $('<li>');
            var message = 'گزینه ای با عنوان <b>' + text + '</b> یافت نشد. ';
            li.html(message);
            if (options['onAddButtonClick']) {
                var addButton = $('<button type="button">').addClass('btn btn-xs btn-info').text('افزودن به لیست').click(function(event) {
                    options['onAddButtonClick'](text, event);
                    inputSearchElement.val('');
                });
                li.append(addButton);
            }
            resultsElement.append(li);
        }

        active(1);
    };

    var show = function() {
        if(currentElement.hasClass('singleselect-dropdown')) {
            dropElement.css('top', containerElement.height());
        }
        else if(currentElement.hasClass('singleselect-dropup')) {
            dropElement.css('bottom', containerElement.height() + 2);
        }
        else if(currentElement.hasClass('singleselect-dropleft')) {
            dropElement.css('right', containerElement.width() + 2);
            dropElement.css('top', -2);
        }
        else if(currentElement.hasClass('singleselect-dropright')) {
            dropElement.css('left', containerElement.width() + 2);
            dropElement.css('top', -2);
        }

        dropElement.fadeIn('fast', function() {
            inputSearchElement.focus();
            find(inputSearchElement.val());
        });
    };

    var hide = function(callback) {
        dropElement.fadeOut('fast', callback);
        activeIndex = 0;
    };

    var select = function(selectedElement) {
        var item;
        var $selectedElement = $(selectedElement);

        for (var i = 0; i < items.length; i++) {
            item = items[i];
            if (item.optionIndex === Number($selectedElement.data('option-index'))) {
                headElement.find('> span.labels').text(item.caption);
                item.option.selected = true;
                item.selected = true;
                currentElement.change();
            }
            else {
                item.option.selected = false;
                item.selected = false;
            }
        }

        hide(function(){
            headElement.focus();
        });
    };

    var active = function(index) {
        if (index)
            activeIndex = index;

        resultsElement.children('li.active').removeClass('active');
        resultsElement.children('li:nth-child(' + activeIndex + ')').addClass('active');
    };

    var refresh = function() {
        
        collect();

        headElement.children('span.labels').html('&nbsp;');

        var item;
        for (var i = 0; i < items.length; i++) {
            item = items[i];
            if (item.selected) {
                headElement.find('> span.labels').text(item.caption);
            }
        }
    };

    init();

    return {
        refresh: function() {
            refresh();
            find(inputSearchElement.val());
            inputSearchElement.focus();
        }
    };
}

//jQuery Plugins
/**
 *
 * @param {Object} options
 * @returns {$.fn@call;each}
 */
$.fn.singleSelect = function(options) {
    return this.each(function() {
        var $this = $(this);
        var singleSelect = $this.data('single-select');
        if (!singleSelect) {
            $this.data('single-select', (singleSelect = SingleSelect(this, options)));
        }
        if (typeof options === 'string') {
            singleSelect[options]();
        }
    });
};